/****************************************************************/

/****************************************************************/
#include "HC-SR04.h"
#include "globalVar.h"
#include "lora.h"
#include "gpsData.h"
#include "ArduinoLowPower.h"

/****************************************************************/

/****************************************************************/
void setup()
{
	pinMode(LED_BUILTIN, OUTPUT);
	Serial.begin(115200);
	if(!Serial)delay(100);

	configDistance(9,8,7,2000);

	configGPS();

	//configLora();
	//delay(5000);
}
/****************************************************************/

/****************************************************************/
void loop()
{
	static String ValidValue;
	String gpsString = SetTxData();
	delay(5000);

	getDistance(&distance);
	
	if(gpsString != "") ValidValue = gpsString;
	else ValidValue = String(distance);
	
	//sendLora(ValidValue);
	//LowPower.deepSleep(5000);
}
/****************************************************************/