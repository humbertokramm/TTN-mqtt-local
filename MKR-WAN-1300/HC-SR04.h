/****************************************************************/
//	Ultrasom
/****************************************************************/
// defines variables
int _trigPin; 
int _echoPin;
int _time_amostra;
int _gnd;

/****************************************************************/
void configDistance(int gnd, int trigPin, int echoPin, int time_amostra) {
	_trigPin = trigPin; 
	_echoPin = echoPin;
	_time_amostra = time_amostra;
	_gnd = gnd;
	pinMode(_gnd, OUTPUT); // Sets the trigPin as an OUTPUT
	pinMode(_trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
	pinMode(_echoPin, INPUT); // Sets the echoPin as an INPUT
	
	
	digitalWrite(trigPin, HIGH);
	digitalWrite(_gnd, HIGH);
}
/****************************************************************/
void getDistance(int * value) {
	static unsigned long last = millis();
	long duration; // variable for the duration of sound wave travel
	if (millis() - last > _time_amostra){
		last = millis();
		
		// Clears the trigPin condition
		digitalWrite(_trigPin, LOW);
		delayMicroseconds(2);
		// Power on module
		digitalWrite(_gnd, LOW);
		delay(120);
		
		// Sets the trigPin HIGH (ACTIVE) for 10 microseconds
		digitalWrite(_trigPin, HIGH);
		delayMicroseconds(10);
		digitalWrite(_trigPin, LOW);
		// Reads the echoPin, returns the sound wave travel time in microseconds
		duration = pulseIn(_echoPin, HIGH);
		
		// Power off module
		digitalWrite(_gnd, HIGH);
		digitalWrite(_trigPin, HIGH);
		
		// Calculating the distance
		*value = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
		// Displays the distance on the Serial Monitor
		Serial.print("Distance: ");
		Serial.print(*value);
		Serial.println(" cm");
	}
}
/****************************************************************/