#include "MKRWAN.h"
#include "arduino_secrets.h"


LoRaModem modem;


// Please enter your sensitive data in the Secret tab or arduino_secrets.h
//Device Name
String _devEui = "1234567890123456";
//#define JOIN_MODE   "ABP"
#define JOIN_MODE   "OTAA"
//OTAA
String _appEui = SECRET_APP_EUI_OTAA;
String _appKey;
//ABP
String _nwkSKey;
String _appSKey;
String _devAddr;

//#define REGION  US915_HYBRID
#define REGION  US915

int connected = false;
unsigned long int tempo;
unsigned long int Contador;
#define TIME_NEW_MSG  60000  // em milisegundos

typedef enum
{
	SEND_BLK,
	TX_BLK,
	JOIN_BLK
} _blinkMsg;
/****************************************************************/

/****************************************************************/
void msgBlink(_blinkMsg value)
{
	int i;
	switch(value)
	{
		case SEND_BLK:
			for(i = 0 ; i < 3; i++)
			{
				digitalWrite(LED_BUILTIN,HIGH);
				delay(200);
				digitalWrite(LED_BUILTIN,LOW);
				delay(800);
			}
			break;

		case JOIN_BLK:
			for(i = 0 ; i < 10; i++)
			{
				digitalWrite(LED_BUILTIN,HIGH);
				delay(200);
				digitalWrite(LED_BUILTIN,LOW);
				delay(800);
			}
			break;

		case TX_BLK:
			for(i = 0 ; i < 1; i++)
			{
				digitalWrite(LED_BUILTIN,HIGH);
				delay(500);
				digitalWrite(LED_BUILTIN,LOW);
				delay(500);
			}
			break;
	}
	delay(3000);
}
/****************************************************************/

/****************************************************************/
void SendTxData(String msg)
{
	//modem.setPort(14);
	msgBlink(TX_BLK);

	modem.beginPacket();
	modem.print(msg);
	if(modem.endPacket(true))Serial.println("Message sent correctly!");
	else
	{
		Serial.println("Error sending message :(");
		Serial.println("(you may send a limited amount of messages per minute, depending on the signal strength");
		Serial.println("it may vary from 1 message every couple of seconds to 1 message every minute)");
	}
	delay(1000);
	if (!modem.available()) {
		Serial.println("Talvez o seu dado não tenha sido enviado, ou essa seja a primeira mensagem enviada com sucesso");
		digitalWrite(LED_BUILTIN,LOW);
	return;
	}
	else 
	{
		char rcv[64];// = {' ',' ',' ',' ',' '};
		int i = 0;
		while (modem.available()) 
		{
			rcv[i++] = (char)modem.read();
		}
		Serial.print("");
		Serial.println(rcv);
		msgBlink(SEND_BLK);
		digitalWrite(LED_BUILTIN,HIGH);
	}

}
/****************************************************************/

/****************************************************************/
void JoinAgain()
{
  static long int count = 0;
  String temp ="1234567890123456";
  String buf = "1234567890123456";// = __devEui;


  temp = SECRET_ADDR_eui_1;
  for(int i = 0;i<16;i++) buf[i] = temp[i+3];

  
  if(_devEui == buf)
  //if(_devEui == temp)
  {
    Serial.println("Tinkerman 01");
    _appKey = SECRET_APP_KEY_1;
    _devEui = SECRET_ADDR_EUI_1;

    _nwkSKey = SECRET_NWK_S_KEY_1;
    _appSKey = SECRET_APP_S_KEY_1;
    _devAddr = SECRET_DEV_ADDR_1;
  }

  temp = SECRET_ADDR_eui_2;
  for(int i = 0;i<16;i++) buf[i] = temp[i];//+3];

  if(_devEui == buf) 
  {
    Serial.println("Tinkerman 02");
    _appKey = SECRET_APP_KEY_2;
  }
  
  

  Serial.print("Tentativa: ");
  Serial.println(++count);

  Serial.print("Entrando pelo modo ");
  
  if(JOIN_MODE == "OTAA")
  {
    Serial.println(JOIN_MODE);
    Serial.print("_appKey: ");
    Serial.println(_appKey);
    Serial.print("_devEui: ");
    Serial.println(_devEui);
    
    connected = modem.joinOTAA(_appEui, _appKey);
    //connected = modem.RefreshOTAA(_appEui, _appKey);
    
    if(connected)msgBlink(JOIN_BLK);
  }
  else if(JOIN_MODE == "ABP")
  {
    Serial.println(JOIN_MODE);
    Serial.print("devAddr: ");
    Serial.println(_devAddr);
    Serial.print("_nwkSKey: ");
    Serial.println(_nwkSKey);
    Serial.print("appSKey: ");
    Serial.println(_appSKey);

    connected = modem.joinABP(_devAddr, _nwkSKey, _appSKey);
    if(connected)msgBlink(JOIN_BLK);
  }
  
  
  //Força a autenticação
  connected = true;

  if(connected)Serial.println("Conectado");
  else Serial.println("Mais sorte na proxima vez :(");

  // NOTE: independently by this setting the modem will
  // not allow to send more than one message every 2 minutes,
  // this is enforced by firmware and can not be changed.
  
}
/****************************************************************/

/****************************************************************/
void configLora(){
	// change this to your regional band (eg. US915, AS923, ...)
	if (!modem.begin(REGION)) {
		Serial.println("Failed to start module");
		while (1) {}
	}

	Serial.print("Your module version is: ");  
	Serial.println(modem.version());
	Serial.print("Your device EUI is: ");
	_devEui = modem.deviceEUI();
	Serial.println(_devEui);

	// Set poll interval to 60 secs.
	modem.minPollInterval(60);

	//Dá um join na rede
	JoinAgain();

	Serial.println(modem.power(PABOOST, 18) );
}
/****************************************************************/

/****************************************************************/
void sendLora(String ValidValue){
	static unsigned long last = millis();
	if (millis() - last > TIME_NEW_MSG){
		last = millis();

		Serial.println(ValidValue);
		if(connected)SendTxData(ValidValue);
		else JoinAgain();

	}
}
/****************************************************************/