#include <TinyGPS.h>//incluimos TinyGPS

#define serialgps Serial1

TinyGPS gps;
/****************************************************************/
String SetTxData()
{
	int year, sat;
	byte month, day, hour, minute, second, hundredths;
	unsigned long chars;
	unsigned short sentences, failed_checksum;
	String dataout = "";
	char temp[20];
	float latitude, longitude, alt;
	static float lastLat,lastLong;

	if(serialgps.available()) 
	{
		int c = serialgps.read(); 
		if(gps.encode(c)) 
		{
			gps.f_get_position(&latitude, &longitude);
			gps.crack_datetime(&year,&month,&day,&hour,&minute,&second,&hundredths);
			gps.stats(&chars, &sentences, &failed_checksum);
			alt = gps.f_altitude();
			sat = gps.satellites();

			//Posição
			if((lastLat == latitude) && (lastLong == longitude) ) return dataout;
			lastLat = latitude;
			lastLong = longitude;

			sprintf(temp, "N: %Lu ", Contador);
			dataout += temp;

			//Prepara o link
			dataout += " maps.google.com/maps?q=";

			sprintf(temp, "%f", latitude);
			dataout += temp;
			sprintf(temp, ",%f", longitude);
			dataout += temp;


			return dataout;

			//Data
			dataout += ",Data:";
			dataout += day;
			dataout += "/";
			dataout += month;
			dataout += "/";
			dataout += year;

			//Hora
			dataout += ",Hora:";
			dataout += hour;
			dataout += ":";
			dataout += minute;
			dataout += ":";
			dataout += second;

			//Altitude
			dataout += ",Alt:";
			dataout += alt;

			//Numero de Satelites
			dataout += ",Sat:";
			dataout += sat;
			return dataout;
		}
	}
	return dataout;
}
/****************************************************************/

/****************************************************************/
void configGPS(){
	serialgps.begin(9600);
}
/****************************************************************/